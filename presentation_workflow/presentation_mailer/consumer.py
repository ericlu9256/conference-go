import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    # # TODO (Eric): check loads function
    # name = json.loads(body["name"])
    # title = json.loads(body["title"])
    # email = json.loads(body["email"])
    presentation_info = json.loads(body)
    send_mail(
        "Subject: Your presentation has been accepted",
        f"{presentation_info['presenter_name']}, we're happy to tell you that your presentation {presentation_info['title']} has been accepted",
        "admin@conference.go",
        [presentation_info["presenter_email"]],
        fail_silently=False,
    )

    print("  Received %r" % body)


def process_approval(ch, method, properties, body):
    # # TODO (Eric): check loads function
    # name = json.loads(body["name"])
    # title = json.loads(body["title"])
    # email = json.loads(body["email"])
    presentation_info = json.loads(body)
    send_mail(
        "Subject: Your presentation has been rejected",
        f"{presentation_info['presenter_name']}, we're sad to tell you that your presentation {presentation_info['title']} has been accepted",
        "admin@conference.go",
        [presentation_info["presenter_email"]],
        fail_silently=False,
    )

    print("  Received %r" % body)


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
